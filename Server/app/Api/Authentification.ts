import * as Protocol from './../Protocol/Authentification';
import * as Datastore from 'nedb';
import * as session from 'express-session';
import { DBs } from './dbLib';
import { ResponseType } from './../Protocol/Protocol'
import {
  Router,
  json as expressJson
} from 'express';
import * as Hashes from 'jshashes';

export class Authentification {
  private _router;
  private _hashEngine = new Hashes.SHA256;

  constructor() {
    this._router = Router();
    this._router.use(expressJson());
    this._router.post('/login', this.login.bind(this));
    this._router.post('/register', this.register.bind(this));
    this._router.get('/check', this.check.bind(this));
    this._router.get('/logout', this.logout.bind(this));

    this._router.get('/', (req, resp) => {
      resp.json({message: "Welcome to the Auth service :)"});
    });
  }

  router(): Router { return this._router; }

  check(req, resp) {
    DBs.sessions.findOne({sessionId: req.sessionID}, (err, doc) => {
      if (doc === null) {
        resp.json({
          type: ResponseType.REJECT
        } as Protocol.Login.CheckResponse);
        return;
      }
      DBs.users.findOne({ _id: doc.user }, (err, userdoc) => {
        if (doc !== null) {
          resp.json({
            type: ResponseType.ACCEPT,
            username: userdoc.username
          } as Protocol.Login.CheckResponse);
        } else {
          resp.json({
            type: ResponseType.REJECT,
            message: "Unknow user id"
          } as Protocol.Login.CheckResponse);
        }
      });
    });
  }

  logout(req, resp) {
    DBs.sessions.findOne({sessionId: req.sessionID}, (err, doc) => {
      if (doc !== null) {
        DBs.sessions.remove({sessionId: req.sessionID}, () => {
          resp.send("Done");
        });
      } else {
        resp.send("Done");
      }
    });
  }
  
  login(req, resp) {
    let response = {} as Protocol.Login.Response;
    let request: Protocol.Login.Request = req.body;

    DBs.users.findOne({ username: request.name, password: request.pass }, (err, doc: any) => {
      if (doc !== null) {
        DBs.sessions.update(
        {sessionId: req.sessionID},
        {sessionId: req.sessionID, user: doc._id},
        {upsert: true}, (err) => {
          if (err) {
            response.type = ResponseType.REJECT;
            response.message = "DB error: " + err.message; 
          } else {
            response.type = ResponseType.ACCEPT;
            response.message = "Success";
          }
          resp.json(response);
        });
      } else {
        response.type = ResponseType.REJECT;
        response.message = "Bad username or password";
        resp.json(response);
      }
    });
  }

  register(req, resp) {
    let response = {} as Protocol.Register.Response;
    let request: Protocol.Register.Request = req.body;

    let usernameCheck = checkForValidUsername(request.name);
    if (usernameCheck.result === false) {
      response.type = ResponseType.REJECT;
      response.message = usernameCheck.message; 
      resp.json(response);
      return;
    }
    let passwordCheck = checkForValidPassword(request.pass);
    if (passwordCheck.result === false) {
      response.type = ResponseType.REJECT;
      response.message = passwordCheck.message; 
      resp.json(response);
      return;
    }
    DBs.users.findOne({ username: request.name } , (err, doc) => {

      if (doc !== null) {
        response.type = ResponseType.REJECT;
        response.message = "Already registered"; 
        resp.json(response);
      } else {
        DBs.users.create(
        { username: request.name, password: this._hashEngine.b64(request.pass) }, (err, doc) => {
          if (err) {
            response.type = ResponseType.REJECT;
            response.message = "DB error: " + err.message; 
          } else {
            response.type = ResponseType.ACCEPT;
            response.message = "Success";
          }
          resp.json(response);
        });
      }
    })
  }
}

function checkForValidUsername(username: string): {result: boolean, message?: string} {
  const pwdarray = [
    {
      message: "Username must be at least 2 character(s) long.",
      expression: "^(.){2,}$"
    },
    {
      message: "Username must be alphanumeric [a-z A-Z 0-9 _].",
      expression: "^[a-zA-Z0-9_]*$"
    }
  ]
  for (let i = 0; i < pwdarray.length; i++) {
    let regexp = new RegExp(pwdarray[i].expression);
    if (!regexp.test(username)) {
      return {
        result: false,
        message: pwdarray[i].message
      }
    }
  }
  return {result: true};
}

function checkForValidPassword(password: string): {result: boolean, message?: string} {
  const pwdarray = [
    {
      message: "Password must be at least 5 character(s) long.",
      expression: "^(.){5,}$"
    },
    {
      message: "Password must be alphanumeric [a-z A-Z 0-9 _].",
      expression: "^[a-zA-Z0-9_]*$"
    }
  ]
  for (let i = 0; i < pwdarray.length; i++) {
    let regexp = new RegExp(pwdarray[i].expression);
    if (!regexp.test(password)) {
      return {
        result: false,
        message: pwdarray[i].message
      }
    }
  }
  return {result: true};
}