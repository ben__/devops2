import * as Interfaces from '../Common/interfaces'
import * as express from 'express';
import { BasicResponse, ResponseType } from '../Protocol/Protocol';
import { DBs } from './dbLib';

function rawBody(req, res, next) {
  req.setEncoding('utf8');
  req.rawBody = '';
  req.on('data', function(chunk) {
    req.rawBody += chunk;
  });
  req.on('end', function(){
    next();
  });
}

export class UserConfig extends Interfaces.RoutedObject {
  constructor(path: string = "/user-config") {
    super(express.Router(), path);

    console.log(path);
    this._router.use(this.loginRequired());
    this._router.use(rawBody);

    this._router.get("/get", this._getUserConfig.bind(this));
    this._router.post("/set", this._setUserConfig.bind(this));
  }

  private async _getUserConfig(req: express.Request, res: express.Response) {
    res.json({
      type: ResponseType.ACCEPT,
      data: req["dbUserDef"].userconfig || "[]"
    } as BasicResponse);
  }

  private _setUserConfig(req: express.Request, res: express.Response) {
      DBs.users.findByIdAndUpdate(req['dbUserDef']._id.toString(),
      { $set: { userconfig: (req as any).rawBody} }).then((res_, err) => {
        if (err) {
          res.json({
            type: ResponseType.REJECT,
            message: "DB error: " + err.message
          } as BasicResponse);
        } else {
          res.json({
            type: ResponseType.ACCEPT,
            message: "done"
          } as BasicResponse);
        }
      });
  }
}