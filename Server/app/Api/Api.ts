import * as Interfaces from '../Common/interfaces'
import * as express from 'express';
import { ServiceBase } from '../Services/ServiceBase';
import * as _ from 'lodash';
import { Authentification } from './Authentification'
import { WidgetSheetResponse } from './../Protocol/Api';
import { ResponseType } from '../Protocol/Protocol';
import { BasicResponse } from '../Protocol/Protocol';
import { UserConfig } from './UserConfig';

class Api extends Interfaces.RoutedObject implements Interfaces.Serialisable {
  private _services: Array<ServiceBase>;
  private _auth: Authentification;
  private _userConfig: UserConfig;

  public static create(router: express.Router) {
    return new Api(router);
  }

  constructor(router: express.Router) {
    super(router, "/api");
    this._services = [];  
    this._auth = new Authentification();
    this._userConfig = new UserConfig();

    this._router.get('/', (req, resp) => {
      resp.json({
        type: ResponseType.ACCEPT,
        message: "Welcome to the Epitech Dashboard Api :3"
      } as BasicResponse);
    });
    this._router.use('/auth', this._auth.router());
    this._router.use(this._userConfig.path(), this._userConfig.router());
    this._router.use((req, res, next) => {
      if (req.path === '.widgetssheet') {
        
      } else {
        next();
      }
    })
    this._router.get('/widgetssheet', this._getWidgetInfos.bind(this));
    // this._router.get('*', (req, res) => {
    //   res.status(404).json({
    //     type: ResponseType.REJECT,
    //     message: "Api route not found"
    //   } as BasicResponse)
    // });
  }

  private _getWidgetInfos(req: express.Request, res: express.Response) {
    let onUserFound = (user) => {
      let servicesArray: Array<Promise<{}>> = [];
      _.each(this._services, (service: ServiceBase) => {
        servicesArray.push(service.widgetInfo(user._id || undefined));
      });
      Promise.all(servicesArray).then((sheets) => {
        res.json({
          type: ResponseType.ACCEPT,
          sheet: sheets
        } as WidgetSheetResponse);
      });
    }
    this.getUser(req).then((user) => {
      onUserFound(user);
    }, () => {
      onUserFound({});
    })
  }

  registerService(service: ServiceBase) {
    this._router.use(service.path(), service.router());
    this._services.push(service);
  }

  about() {
    let servicesArray: Array<{}> = [];
    _.each(this._services, (service: ServiceBase) => {
      servicesArray.push(service.about());
    });
    return servicesArray;
  }

  widgetInfo() {return Promise.resolve({})}
}

export let ApiInstance = Api.create(express.Router());