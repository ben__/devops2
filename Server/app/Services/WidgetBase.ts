import { RoutedObject, Serialisable } from '../Common/interfaces';
import { Router } from 'express';

export abstract class WidgetBase extends RoutedObject implements Serialisable {
  constructor(router: Router,
              path: string) { super(router, path); }

  abstract about(): {};
  abstract widgetInfo(userId: string): Promise<{}>;
}