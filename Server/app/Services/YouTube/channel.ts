import { YouTubeWidget } from './WidgetYoutubeBase';
import { Router } from 'express';
import { ResponseType } from '../../Protocol/Protocol';
import { parse as HtmlParser } from 'node-html-parser';
import * as https from 'https';
import * as _ from 'lodash';

class YouTubeChannel extends YouTubeWidget {
  constructor(router: Router) {
    super(router, "/channel");
    
    router.get('/get', (req, baseResp) => {
      if (req.query.url !== undefined) {
        https.get(`${req.query.url}/about`,
          (resp) => {
            let data = '';
            resp.on('data', (chunk) => {
              data += chunk;
            });
            resp.on('end', () => {
              try {
                let fdata = this._parseData(data);
                
                baseResp.json({
                  type: ResponseType.ACCEPT,
                  data: fdata
                });
              } catch (e) {
                baseResp.json({
                  type: ResponseType.REJECT,
                  message: "Parsing failed!"
                });
              }
            });
    
            resp.on('error', function(err) {
              baseResp.json({
                type: ResponseType.REJECT,
                message: "unable to connect to remote host"
              });
            });
          });
      } else {
        console.log('Channel missing !');
        baseResp.json({
          type: ResponseType.REJECT,
          message: "missing arg"
        });
      }
    });
  }

  private _parseData(data: string) {
    let parsed = HtmlParser(data);

    let stats_elem = (parsed as any).querySelector('body #content .branded-page-v2-container' +
                                    ' .branded-page-v2-col-container-inner' +
                                    ' .about-metadata-stats .about-stats');
    let title = (parsed as any).querySelector('.qualified-channel-title-text');
    //let image = HtmlParser((parsed as any).querySelector('.channel-header-profile-image').toString() as string);
    let image = (parsed as any).querySelector('.channel-header-profile-image') as any as HTMLElement;
    let stats = _.map((stats_elem as any).querySelectorAll(".about-stat b"),
                    (value) => {
                  return value.rawText
                });
    return {
      subs: stats[0],
      views: stats[1],
      title: title.rawText,
      image: (image.attributes as any).src
    };
  }

  about() {
    return {
      "name": "YouTube Channel",
      "description": "Affiche les infos d'une chaîne Youtube",
      "params": [
        {
          "name": "url",
          "type": "string"
        }
      ]
    }
  }

  widgetInfo() {
    return Promise.resolve({
      name: "Channel",
      iconUrl: "/assets/youtube/icon.png",
      description: "Get infos for a channel"
    });
  }
}

export let YouTubeChannelInstance = new YouTubeChannel(Router());