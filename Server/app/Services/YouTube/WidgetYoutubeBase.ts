import { Router } from 'express';
import { WidgetBase } from '../WidgetBase';
import { YouTubeServiceInstance } from './service';

export abstract class YouTubeWidget extends WidgetBase {
  protected _service = YouTubeServiceInstance;

  constructor(router: Router, path: string) {
    super(router, path);
    this._service.register(this);
  }
}