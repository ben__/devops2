import { Router } from 'express';
import { ServiceBase } from '../ServiceBase';
import * as _ from 'lodash';
import { ResponseType } from '../../Protocol/Protocol';


class DiversService extends ServiceBase {
  constructor(router: Router) {
    super("Divers", router, "/divers");
    
    this._color = "#585858";
    this._router.use(this.loginRequired());
    router.get('/', (req, resp) => {
      resp.json({
        message: "Welcome to the Divers service my friend :)"
      });
    });
  }

  isRegistered() { return Promise.resolve(true); }

  getApiInfo() {
    return {}
  }
}

export let DiversServiceInstance = new DiversService(Router());
