import { DiversWidget } from './WidgetDiversBase';
import { Router } from 'express';
import { ResponseType } from '../../Protocol/Protocol';
import * as https from 'https';
import * as xml2js from 'xml2js';
import * as _ from 'lodash';

class AtomWidget extends DiversWidget {
  constructor(router: Router) {
    super(router, "/atom");
    
    router.get('/get', (req, baseResp) => {
      if (req.query.url !== undefined) {
        https.get(req.query.url as string, (resp) => {
          let data = '';
          resp.on('data', (chunk) => {
            data += chunk;
          });
          resp.on('end', () => {
            xml2js.parseString(data, function (err, result) {
              if (err) {
                baseResp.json({
                  type: ResponseType.REJECT,
                  message: "failed to parse Atom stream",
                  baseError: err
                });
                return;
              }
              result = result.feed;
              let n = req.query.n || result.item.length;
              result.item = result.entry.slice(0, n);

              let data = {
                title: result.title[0] || "",
                logo: "",
                link: "",
                items: _.map(result.item, (itm) => {
                  let desc = {}
                  if (itm.content[0])
                    desc = {
                      content: itm.content[0]._,
                      type: itm.content[0].$.type
                    }
                  return {
                    desc: desc,
                    title: itm.title[0] || "",
                    link: (((itm.link[0] || {}).$) || {}).href || "",
                  }
                })
              }

              baseResp.json({
                type: ResponseType.ACCEPT,
                data: data
              });
            });
          });
  
          resp.on('error', function(err) {
            baseResp.json({
              type: ResponseType.REJECT,
              message: "unable to connect to remote host",
              baseError: err
            });
          });
        });
        
      } else {
        console.log('url missing !');
        baseResp.json({
          type: ResponseType.REJECT,
          message: "missing arg url"
        });
      }
    });
  }

  about() {
    return {
      "name": "Atom",
      "description": "Affiche flux atom R, N lignes",
      "params": [
        {
          "name": "url",
          "type": "string"
        },
        {
          "name": "n",
          "type": "number"
        }
      ]
    }
  }

  widgetInfo() {
    return Promise.resolve({
      name: "Atom",
      iconUrl: "/assets/divers/rss-atom.png",
      description: "Get items for a search"
    });
  }
}

export let AtomWidgetInstance = new AtomWidget(Router());