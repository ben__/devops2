import { Router } from 'express';
import { WidgetBase } from '../WidgetBase';
import { DiversServiceInstance } from './service';

export abstract class DiversWidget extends WidgetBase {
  protected _service = DiversServiceInstance;

  constructor(router: Router, path: string) {
    super(router, path);
    this._service.register(this);
  }
}