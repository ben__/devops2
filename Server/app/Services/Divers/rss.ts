import { DiversWidget } from './WidgetDiversBase';
import { Router } from 'express';
import { ResponseType } from '../../Protocol/Protocol';
import * as https from 'https';
import * as xml2js from 'xml2js';
import * as _ from 'lodash';

class RssWidget extends DiversWidget {
  constructor(router: Router) {
    super(router, "/rss");
    
    router.get('/get', (req, baseResp) => {
      if (req.query.url !== undefined) {
        https.get(req.query.url as string, (resp) => {
          let data = '';
          resp.on('data', (chunk) => {
            data += chunk;
          });
          resp.on('end', () => {
            xml2js.parseString(data, function (err, result) {
              if (err) {
                baseResp.json({
                  type: ResponseType.REJECT,
                  message: "failed to parse rss stream",
                  baseError: err
                });
                return;
              }
              result = result.rss.channel[0];
              let n = req.query.n || result.item.length;
              result.item = result.item.slice(0, n);

              let data = {
                title: result.title[0] || "",
                logo: (((result.image || [])[0] || {}).url || [""])[0],
                link: (result.link || [])[0] || "",
                items: _.map(result.item, (itm) => {
                  return {
                    desc: itm.description[0] || "",
                    title: itm.title[0] || "",
                    link: itm.link[0] || "",
                  }
                })
              }

              baseResp.json({
                type: ResponseType.ACCEPT,
                data: data
              });
            });
          });
  
          resp.on('error', function(err) {
            baseResp.json({
              type: ResponseType.REJECT,
              message: "unable to connect to remote host",
              baseError: err
            });
          });
        });
        
      } else {
        console.log('url missing !');
        baseResp.json({
          type: ResponseType.REJECT,
          message: "missing arg url"
        });
      }
    });
  }

  about() {
    return {
      "name": "RSS",
      "description": "Affiche flux rss R, N lignes",
      "params": [
        {
          "name": "url",
          "type": "string"
        },
        {
          "name": "n",
          "type": "number"
        }
      ]
    }
  }

  widgetInfo() {
    return Promise.resolve({
      name: "RSS",
      iconUrl: "/assets/divers/rss-atom.png",
      description: "Get items for a search"
    });
  }
}

export let RssWidgetInstance = new RssWidget(Router());