import { RoutedObject, Serialisable } from '../Common/interfaces';
import { Router } from 'express';
import { WidgetBase } from './WidgetBase';
import { ApiInstance } from '../Api/Api';
import * as _ from 'lodash';

export abstract class ServiceBase extends RoutedObject implements Serialisable {
  protected _widgets: Array<WidgetBase> = [];
  protected _api = ApiInstance;
  protected _loginFields = [
    {
      name: "identifiant",
      placeholder: "Username",
      type: "text"
    },
    {
      name: "pass",
      placeholder: "Password",
      type: "password"
    }
  ];
  protected _color = "black";

  abstract isRegistered(userId: string): Promise<boolean>;

  constructor(protected _name: string,
              router: Router,
              path: string) {
    super(router, path);
    ApiInstance.registerService(this);
  }
  
  register(w: WidgetBase) {
    this._router.use(w.path(), w.router());
    this._widgets.push(w);
  }

  getApi() {
    return ApiInstance;
  }

  about() {
    let result: Array<{}> = [];

    _.each(this._widgets, (widget: WidgetBase) => {
      result.push(widget.about());
    });
    return {
      name: this._name,
      widgets: result
    };
  }

  widgetInfo(userId: string) {
    return new Promise((success) => {
      this.isRegistered(userId).then((registered) => {
        let result = {
          path: this._path,
          registered: registered,
          fields: this._loginFields,
          name: this._name,
          color: this._color,
          widgets: [] as Array<{}>
        }
        let wids = new Array<Promise<{}>>();
        _.each(this._widgets, (widget: WidgetBase) => {
          wids.push(widget.widgetInfo(userId));
        });
        Promise.all(wids).then((widSheets) => {
          result.widgets = widSheets;
          success(result);
        });
      })
    })
  }
}