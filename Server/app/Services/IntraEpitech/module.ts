import { IntraEpitechWidget } from './WidgetIntraEpitechBase';
import { ModuleIntraEpitechResponse } from '../../Protocol/IntraEpitech';
import { Router } from 'express';
import { ResponseType } from '../../Protocol/Protocol';
import * as https from 'https';
import * as moment from 'moment';

class ModuleIntraEpitech extends IntraEpitechWidget {
  constructor(router: Router) {
    super(router, "/module");

    router.get('/get', (req, baseResp) => {
      let user = req["dbUserDef"];
      this._service.getUserInfos(user._id).then((doc) => {
        if (req.query.module !== undefined) {
          https.get(`https://intra.epitech.eu/auth-${doc.key}/?format=json`,
            (resp) => {
              let data = '';
              resp.on('data', (chunk) => {
                data += chunk;
              });
              resp.on('end', () => {
                  let fdata = this._parseData(req.query.module as string, data);
    
                  baseResp.json({
                  type: ResponseType.ACCEPT,
                  data: fdata
                } as ModuleIntraEpitechResponse);
              });
      
              resp.on('error', function(err) {
                baseResp.json({
                  type: ResponseType.REJECT,
                  message: "unable to connect to remote host"
                } as ModuleIntraEpitechResponse);
              });
            });
        } else {
          baseResp.on('error', function(err) {
            baseResp.json({
              type: ResponseType.REJECT,
              message: "invalid module name"
            } as ModuleIntraEpitechResponse);
          });
        }
      }, () => {
        baseResp.json({
          type: ResponseType.REJECT,
          message: "Not connected to Epitech"
        } as ModuleIntraEpitechResponse);
      })
    });
  }

  private _parseData(module: string, data: string) {
    let fdata = [];

    (JSON.parse(data)["board"]["projets"] || []).forEach(e => {
      if (e) {
        if (e["title_link"].match(`B-${module}`)) {
          e.timeline_start = moment(e.timeline_start, 'DD/MM/YYYY, hh:mm').format('LLLL');
          e.timeline_end = moment(e.timeline_end, 'DD/MM/YYYY, hh:mm').format('LLLL');
          e.timeline_barre = Number.parseFloat(e.timeline_barre);
          fdata.push(e);
        }
      }
    });
    //24/09/2018, 09:00
    return fdata;
  }

  about() {
    return {
      "name": "module_activity",
      "description": "affiche les N prochaines activitées d'un module Epitech M",
      "params": [
        {
          "name": "module",
          "type": "string"
        }
      ]
    }
  }

  widgetInfo() {
    return Promise.resolve({
      name: "Module",
      iconUrl: "/assets/epitech/epitech.jpeg",
      description: "Get activity for a module given"
    });
  }
}

export let ModuleIntraEpitechInstance = new ModuleIntraEpitech(Router());