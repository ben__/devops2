import { Router } from 'express';
import { WidgetBase } from '../WidgetBase';
import { IntraEpitechServiceInstance } from './service';

export abstract class IntraEpitechWidget extends WidgetBase {
  protected _service = IntraEpitechServiceInstance;

  constructor(router: Router, path: string) {
    super(router, path);
    this._service.register(this);
  }
}