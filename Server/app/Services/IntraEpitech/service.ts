import { Router, Request, Response, json as expressJson } from 'express';
import { ServiceBase } from '../ServiceBase';
import { DBs } from '../../Api/dbLib';
import { BasicResponse, ResponseType } from '../../Protocol/Protocol';
import * as Datastore from 'nedb';
import * as _ from "lodash";

class IntraEpitechService extends ServiceBase {
  private _db: Datastore;

  constructor(router: Router) {
    super("Intra Epitech", router, "/intraEpitech");

    this._loginFields = [
      {
        name: "identifiant",
        placeholder: "Auto-login token",
        type: "text"
      }
    ];
    
    this._db = DBs.add(this._name, { fieldName: 'userId', unique: true });

    this._color = "#0769fd";

    router.use(this.loginRequired());
    router.get('/', (req, resp) => {
      resp.json({
        message: "Welcome to the IntraEpitech service my friend :)"
      });
    });
    let _expressJson = expressJson();
    router.use(function(req, res, next) {
      if (req.path === '/login')
        _expressJson(req, res, next);
      else
        next();
    })
    router.post('/login', this.login.bind(this));
  }

  login(req: Request, resp: Response) {
    let user = req["dbUserDef"];
    let data = req["body"];
    if (!data) {
      resp.json({
        type: ResponseType.REJECT,
        message: "Bad data format"
      } as BasicResponse);
      return;
    }
    let reformated = _.reduce(data, (res, curr) => {
      res[curr.name] = curr.value;
      return res;
    }, {}) as any;
    if (!reformated.identifiant) {
      resp.json({
        type: ResponseType.REJECT,
        message: "Bad data format"
      } as BasicResponse);
      return;
    }
    this._db.update(
      {userId: user._id},
      { $set: {key: reformated.identifiant} },
      {upsert: true}, (err) => {
        if (err) {
          resp.json({
            type: ResponseType.REJECT,
            message: "DB Error :" + err.message
          } as BasicResponse);
        } else {
          resp.json({
            type: ResponseType.ACCEPT,
            message: "Done"
          } as BasicResponse);
        }
      })
  }

  isRegistered(userId: string) {
    return new Promise<boolean>((success) => {
      this._db.count({userId: userId}, (err, count) => {
        success(!!count);
      });
    });
  }

  getUserInfos(userId: string) {
    return new Promise<any>((success, fail) => {
      this._db.findOne({userId: userId}, (err, doc) => {
        if (err || doc === null) fail(err); else {
          success(doc);
        }
      });
    });
  }
}

export let IntraEpitechServiceInstance = new IntraEpitechService(Router());
