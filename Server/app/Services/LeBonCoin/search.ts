import { LeBonCoinWidget } from './WidgetLeBonCoinBase';
import { Router } from 'express';
import { ResponseType } from '../../Protocol/Protocol';
import * as leboncoin from 'leboncoin-api';

class LeBonCoinSearch extends LeBonCoinWidget {
  constructor(router: Router) {
    super(router, "/search");
    
    router.get('/get', (req, baseResp) => {
      if (req.query.term !== undefined) {
        var search = new leboncoin.Search()
          .setPage(1)
          .setQuery(req.query.term)
          .setFilter(leboncoin.FILTERS.PARTICULIER)
          .setCategory(this._service.getApiInfo().getCategory(req.query.category as string))
          .setRegion(this._service.getApiInfo().getRegion(req.query.region as string))

      search.run().then(function (data) {
        baseResp.json({
          type: ResponseType.ACCEPT,
          data: data.results.slice(0, req.query.n || 999)
        });
      }, function (err) {
          baseResp.json({
            type: ResponseType.REJECT,
            message: err
          });
      });
      } else {
        console.log('search missing !');
        baseResp.json({
          type: ResponseType.REJECT,
          message: "missing arg"
        });
      }
    });
  }

  about() {
    return {
      "name": "LeBonCoin search",
      "description": "Affiche N derniere ventre pour la recherche R",
      "params": [
        {
          "name": "search",
          "type": "string"
        },
        {
          "name": "n",
          "type": "number"
        },
        {
          "name": "category",
          "type": "string"
        },
        {
          "name": "region",
          "type": "string"
        }
      ]
    }
  }

  widgetInfo() {
    return Promise.resolve({
      name: "Search",
      iconUrl: "/assets/leboncoin/icon.jpg",
      description: "Get items for a search"
    });
  }
}

export let LeBonCoinSearchInstance = new LeBonCoinSearch(Router());