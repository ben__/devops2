import { Router } from 'express';
import { ServiceBase } from '../ServiceBase';
import * as http from 'http';

class WeatherService extends ServiceBase {
  constructor(router: Router) {
    super("Weather", router, "/weather");
    
    this._color = "#50a9b7";
    this._router.use(this.loginRequired());
    router.get('/', (req, resp) => {
      resp.json({
        message: "Welcome to the Weather service my friend :)"
      });
    });
  }

  isRegistered() { return Promise.resolve(true); }

  getApiInfo() {
    return {
      key: "86884cb417a6bcb54f815630856d632b",
      validCod: 200,
      limitCode: 429,
      invalidKeyCode: 401
    }
  }
}

export let WeatherServiceInstance = new WeatherService(Router());
