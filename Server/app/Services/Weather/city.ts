import { WeatherWidget } from './WidgetWeatherBase';
import { WeatherCityResponse } from '../../Protocol/Weather';
import { Router } from 'express';
import * as http from 'http';
import { ResponseType } from '../../Protocol/Protocol';

class WeatherCity extends WeatherWidget {
  constructor(router: Router) {
    super(router, "/city");
    
    router.get('/get', (req, baseResp) => {
      if (req.query.city !== undefined) {
        http.get(`http://api.openweathermap.org/data/2.5/weather?q=${req.query.city}&units=metric&APPID=${this._service.getApiInfo().key}`,
        (resp) => {
          let data = '';
          resp.on('data', (chunk) => {
            data += chunk;
          });
          
          resp.on('end', () => {
            baseResp.json({
              type: ResponseType.ACCEPT,
              data: data
            } as WeatherCityResponse);
          });

          resp.on('error', function(err) {
            baseResp.json({
              type: ResponseType.REJECT,
              message: "unable to connect to remote host"
            } as WeatherCityResponse);
          });
        });
      } else {
        console.log('city missing !');
        baseResp.json({
          type: ResponseType.REJECT,
          message: "missing arg city"
        } as WeatherCityResponse);
      }
    });
  }

  about() {
    return {
      "name": "city_temperature",
      "description": "affiche la température pour une ville",
      "params": [
        {
          "name": "city",
          "type": "string"
        }
      ]
    }
  }

  widgetInfo() {
    return Promise.resolve({
      name: "City",
      iconUrl: "/assets/weather/cityIcon.png",
      description: "Get weather for a given city"
    });
  }
}

export let WeatherCityInstance = new WeatherCity(Router());