import { PopupProperty } from './../global-popup/global-popup.component';
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../Services/api.service';
import { Observable } from 'rxjs';
import { BasicResponse, ResponseType } from '../../Protocol/Protocol';

let __services = [
  {
    name: "Youtube",
    registered: false,
    color: "red",
    widgets: [
      {
        name: "Subscribe",
        iconUrl: "/assets/youtube/youtubeIcon.png",
        description: "Je suis une desc",
      }
    ]
  },
  {
    name: "Twitter",
    registered: true,
    color: "blue",
    widgets: [
      {
        name: "Last Post",
        iconUrl: "/assets/Twitter/lastPostIcon.png",
        description: "Je suis une desc",
      },
      {
        name: "Last Post",
        iconUrl: "/assets/Twitter/lastPostIcon.png",
        description: "Je suis une desc",
      },
      {
        name: "Last Post",
        iconUrl: "/assets/Twitter/lastPostIcon.png",
        description: "Je suis une desc",
      },
      {
        name: "Last Post",
        iconUrl: "/assets/Twitter/lastPostIcon.png",
        description: "Je suis une desc",
      }
    ]
  },
  {
    name: "RSS",
    color: "gray",
    widgets: [
      {
        name: "Flux",
        iconUrl: "/assets/rss/rssIcon.png",
        description: "Je suis une desc"
      }
    ]
  },
  {
    name: "Weather",
    color: "#50a9b7",
    widgets: [
      {
        name: "City",
        iconUrl: "/assets/weather/weatherIcon.png",
        description: "Get wheater for a given city"
      }
    ]
  }

]

export interface ResultData {
  service: any,
  widget: {
    name: string,
    iconUrl: string,
    description: string
  }
}

@Component({
  selector: 'app-widget-selector',
  templateUrl: './widget-selector.component.html',
  styleUrls: ['./widget-selector.component.scss']
})
export class WidgetSelectorComponent extends PopupProperty implements OnInit {
  public services = this._api.getWidgetsSheet();

  constructor(private _api: ApiService) { super() }

  select(service: {}, widgetDef: {}) {
    this.accept({
      service: service,
      widget: widgetDef
    });
  }

  ngOnInit() {
  }

  submitClicked(tab) {
    this._api.loginToService(tab.path, tab.fields).subscribe((res: BasicResponse) => {
      if (res.type = ResponseType.ACCEPT) {
        this.services = this._api.getWidgetsSheet();
      }
    })
  }

}