import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as Protocol from '../../Protocol/Authentification';
import * as Hashes from 'jshashes';
import { WidgetSheetResponse } from '../../Protocol/Api';
import { ResponseType, BasicResponse } from 'src/Protocol/Protocol';
import { Observable, Observer } from 'rxjs';
import { environment } from '../../environments/environment';

interface Options {
  path: string;
  data?: any;
};

enum HttpRequestType {
  GET,
  POST
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private _hashEngine = new Hashes.SHA256;
  public needLogin = new EventEmitter<any>();
  public onLoginSuccess = new EventEmitter<void>();

  constructor(private _http: HttpClient) {}

  private _basic<T, U>(type: HttpRequestType, options: Options | string, onSuccess?: (resp: T, observer: Observer<U>) => void) {
    if (typeof options === 'string') {
      options = {path: options};
    }

    return new Observable((observer) => {
      switch (type) {
        case HttpRequestType.GET:
          var obs = this._http.get<T>((options as Options).path);
          break;
        case HttpRequestType.POST:
          var obs = this._http.post<T>((options as Options).path, (options as Options).data);
          break;
        default:
          throw 'Bad request type!!';
      }
      obs.subscribe((resp) => {
        if ((resp as any as BasicResponse).type === ResponseType.ACCEPT) {
          if (typeof onSuccess !== 'undefined')
            onSuccess(resp, observer);
          else {
            observer.next(resp);
          }
        } else {
          observer.error(resp);
        }
      }, (error) => {
        observer.error(error);
      })
    });
  }

  logout() {
    this._http.get('/api/auth/logout', {responseType:'text'}).subscribe((resp) => {
      this.needLogin.emit();
    });
  }

  login(username: string, password: string) {
    password = this._hashEngine.b64(password);
    let obs = this._http.post<Protocol.Login.Response>('/api/auth/login', {
      name: username,
      pass: password
    } as Protocol.Login.Request);
    obs.subscribe((resp: Protocol.Login.Response) => {
      if (resp.type == ResponseType.ACCEPT) {
        this.onLoginSuccess.emit();
      }
    });
    return obs;
  }

  register(username: string, password: string) {
    return this._http.post<Protocol.Register.Response>('/api/auth/register', {
      name: username,
      pass: password
    } as Protocol.Login.Request);
  }

  isLogged() {
    let promise = new Promise((resolve, reject) => {
      this._http.get<Protocol.Login.CheckResponse>('/api/auth/check')
      .subscribe((resp) => {
        if (resp.type === ResponseType.ACCEPT) {
          this.onLoginSuccess.emit();
          resolve();
        } else { 
          reject(resp);
        }
      }, (error) => {
        reject(error);
      })
    });
    return promise;
  }

  getConfig() {
    return this._basic(HttpRequestType.GET, '/api/user-config/get', (resp: BasicResponse, observer) => {
      observer.next(JSON.parse(resp.data));
    }) as Observable<Array<{}>>;
  }

  setConfig(config: any) {
    return this._basic(HttpRequestType.POST, {
      path: '/api/user-config/set',
      data: config
    }, (resp: BasicResponse, observer) => {
      observer.next(resp.message);
    }) as Observable<string>;
  }

  getWidgetsSheet() {
    return this._basic(HttpRequestType.GET, '/api/widgetssheet', (resp: WidgetSheetResponse, observer) => {
      observer.next(resp.sheet);
    }) as Observable<Array<{}>>;
  }

  weatherCity(city: string) {
    return this._basic(HttpRequestType.GET, `/api/weather/city/get?city=${city}`);
  }

  loginToService(serviceName, fields) {
    return this._basic(HttpRequestType.POST, {
      path: `/api${serviceName}/login`,
      data: fields
    })
  }

  intraEpitech = {
    module: (moduleName: string) => {
      return this._basic(HttpRequestType.GET, `/api/intraEpitech/module/get?module=${moduleName}`);
    },
    user: () => {
      return this._basic(HttpRequestType.GET, `/api/intraEpitech/user/get`);
    }
  }

  public settingsGetOptions(url: string) {
    return this._basic(HttpRequestType.GET, url, (resp: any, observer) => {
      observer.next(resp.data);
    });
  }

  leboncoin = {
    search: (search: string, category: string, region: string, n: any) => {
      return this._basic(HttpRequestType.GET, `/api/leboncoin/search/get?term=${search}&category=${category}&region=${region}&n=${n}`);
    }
  }

  youtube = {
    channel: (url: string) => {
      return this._basic(HttpRequestType.GET, `/api/youtube/channel/get?url=${encodeURIComponent(url)}`);
    }
  }

  divers = {
    rss: (url: string, n: any) => {
      return this._basic(HttpRequestType.GET, `/api/divers/rss/get?url=${encodeURIComponent(url)}&n=${n}`);
    },
    atom: (url: string, n: any) => {
      return this._basic(HttpRequestType.GET, `/api/divers/atom/get?url=${encodeURIComponent(url)}&n=${n}`);
    }
  }
}
