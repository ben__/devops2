import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { WidgetCompBase } from '../dashboard-main/Widgets/WidgetBase';

const REFNAME = "__timer__ref__";

@Injectable({
  providedIn: 'root'
})
export class TimerService {
  constructor() {}

  listen(component: WidgetCompBase, request: (observer) => void) {
    let tRef = { 
      obs: (() => { return new Observable((observer) => {
        let timer;
        let refresh = () => {
          request(observer);
          timer = setTimeout(refresh, component.settings.intervalMs.valueOf());
        };
        tRef.forceRefresh = () => {
          clearTimeout(timer);
          refresh();
        }
        refresh();
      })})()
    } as any;
    component[REFNAME] = tRef;
    return component[REFNAME]
  }

  forceRefresh(component: WidgetCompBase) {
    let tRef = component[REFNAME];
    if (tRef) {
      tRef.forceRefresh();
    }
  }
}
