import {
  AfterContentInit,
  Directive,
  ElementRef,
  Input,
  ApplicationRef,
  EmbeddedViewRef,
  ComponentRef } from '@angular/core';
import { WidgetCompBase } from './Widgets/WidgetBase';
import * as _ from 'lodash';

@Directive({
  selector: '[app-widget-selector]'
})
export class WidgetSelectorDirective implements AfterContentInit {
  constructor(private _appRef: ApplicationRef,
              private _me: ElementRef) {
    _me.nativeElement.style.display = "block";
  }
  @Input("app-widget-component") widgetComponent: any;

  ngAfterContentInit() {
    _.defer(() => {
      this.widgetComponent.parent = this._me.nativeElement;
      this._append(this.widgetComponent.componentRef);
    });
  }

  private _append(componentRef: ComponentRef<WidgetCompBase>) {
    this._appRef.attachView(componentRef.hostView);
    const domElem = (componentRef.hostView as EmbeddedViewRef<any>)
      .rootNodes[0] as HTMLElement;

    (this._me.nativeElement as HTMLElement).appendChild(domElem);
    return componentRef.instance;
  }
}
