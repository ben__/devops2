import { Component, OnInit, ElementRef } from '@angular/core';
import { WidgetCompBase } from '../WidgetBase';
import { ApiService } from 'src/app/Services/api.service';
import { EventService } from 'src/app/Services/event.service';
import { TimerService } from 'src/app/Services/timer.service';
import { GlobalPopupService } from 'src/app/global-popup/global-popup.service';
import { SettingItem, SettingTypes } from 'src/app/widget-settings/widget-settings.component';
import { ResponseType, BasicResponse } from '../../../../Protocol/Protocol';
import * as _ from "lodash";

export class ModuleIntraEpitechResponse extends BasicResponse {
  data: any;
}

@Component({
  selector: 'app-widget-epitech-module',
  templateUrl: './widget-epitech-module.component.html',
  styleUrls: ['./widget-epitech-module.component.scss']
})
export class WidgetEpitechModuleComponent extends WidgetCompBase implements OnInit {
  public errorMessage = "";
  public modules = [];

  constructor(api: ApiService,
              private _me: ElementRef,
              eventService: EventService,
              timer: TimerService,
              popup: GlobalPopupService) {
    super(eventService, timer, popup, api);
    this.settings["moduleName"] = new SettingItem(SettingTypes.STRING, {
      displayName: "Module name"
    }, "");
    this.settings["numberOf"] = new SettingItem(SettingTypes.NUMBER, {
      displayName: "Number of activities displayed"
    }, 5);
    
  }

  ngOnInit() {
    this.listen((observer) => {
      if (this.settings.moduleName && this.settings.moduleName !== "") {
        this._api.intraEpitech.module(this.settings.moduleName).subscribe((data: ModuleIntraEpitechResponse) => {
          observer.next(data);
        });
      } else {
        let data = new ModuleIntraEpitechResponse();
        data.message = "Please give us a module name";
        data.type = ResponseType.REJECT;
        observer.next(data);
      }
    }, (result: ModuleIntraEpitechResponse) => {
      _.defer(() => {
        this.loaded = true;
        (this._me.nativeElement as HTMLElement).style.width = "100%";
      });
      if (result.type === ResponseType.REJECT) {
        this.errorMessage = result.message;
        return;
      } else {
        (result.data as Array<any>).splice(
          this.settings.numberOf,
          (result.data as Array<any>).length - this.settings.numberOf);
        this.modules = result.data as any;
      }
    });
  }

  getColorForPercentage(pct) {
    pct = pct / 100;
    const percentColors = [
      { pct: 0.0, color: { r: 0x00, g: 0xff, b: 0 } },
      { pct: 0.5, color: { r: 0xff, g: 0xff, b: 0 } },
      { pct: 1.0, color: { r: 0xff, g: 0x00, b: 0 } } ];

    for (var i = 1; i < percentColors.length - 1; i++) {
        if (pct < percentColors[i].pct) {
            break;
        }
    }
    let lower = percentColors[i - 1];
    let upper = percentColors[i];
    let range = upper.pct - lower.pct;
    let rangePct = (pct - lower.pct) / range;
    let pctLower = 1 - rangePct;
    let pctUpper = rangePct;
    let color = {
        r: Math.floor(lower.color.r * pctLower + upper.color.r * pctUpper),
        g: Math.floor(lower.color.g * pctLower + upper.color.g * pctUpper),
        b: Math.floor(lower.color.b * pctLower + upper.color.b * pctUpper)
    };
    return 'rgb(' + [color.r, color.g, color.b].join(',') + ')';
  }

}
