import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetEpitechModuleComponent } from './widget-epitech-module.component';

describe('WidgetEpitechModuleComponent', () => {
  let component: WidgetEpitechModuleComponent;
  let fixture: ComponentFixture<WidgetEpitechModuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetEpitechModuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetEpitechModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
