import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetRssComponent } from './widget-rss.component';

describe('WidgetRssComponent', () => {
  let component: WidgetRssComponent;
  let fixture: ComponentFixture<WidgetRssComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetRssComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetRssComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
