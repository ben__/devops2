import { Component, OnInit } from '@angular/core';
import { WidgetCompBase } from '../WidgetBase';
import { ApiService } from 'src/app/Services/api.service';
import { EventService } from 'src/app/Services/event.service';
import { TimerService } from 'src/app/Services/timer.service';
import { GlobalPopupService } from 'src/app/global-popup/global-popup.service';
import { Observer } from 'rxjs';
import { SettingItem, SettingTypes } from 'src/app/widget-settings/widget-settings.component';

@Component({
  selector: 'app-widget-atom',
  templateUrl: './widget-atom.component.html',
  styleUrls: ['./widget-atom.component.scss']
})
export class WidgetAtomComponent extends WidgetCompBase implements OnInit {

  data: any;

  constructor(api: ApiService,
              eventService: EventService,
              timer: TimerService,
              popup: GlobalPopupService) {
    super(eventService, timer, popup, api);

    this.settings["url"] = new SettingItem(SettingTypes.STRING, {
      displayName: "Atom stream url"
    }, "");
    this.settings["numberOf"] = new SettingItem(SettingTypes.NUMBER, {
      displayName: "Number of items displayed"
    }, 5);
  }

  open(url: string) {
    var win = window.open(url, '_blank');
    win.focus();
  }

  parseHTML(content: string) {
    var el = document.createElement('div');
    el.innerHTML = content;

    let text = el.innerText;
    
    el.remove();
    return text;
  }

  ngOnInit() {
    this.listen((observer: Observer<any>) => {
      this._api.divers.atom(this.settings.url._data, this.settings.numberOf).subscribe((data: any) => {
        observer.next(data.data);
      });
    }, (result: any) => {
      this.loaded = true;
      this.data = result;
    });
  }

}
