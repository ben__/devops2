import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetAtomComponent } from './widget-atom.component';

describe('WidgetAtomComponent', () => {
  let component: WidgetAtomComponent;
  let fixture: ComponentFixture<WidgetAtomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetAtomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetAtomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
