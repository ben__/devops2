import { Component, OnInit } from '@angular/core';
import { WidgetCompBase } from '../WidgetBase';
import { ApiService } from 'src/app/Services/api.service';
import { EventService } from 'src/app/Services/event.service';
import { TimerService } from 'src/app/Services/timer.service';
import { GlobalPopupService } from 'src/app/global-popup/global-popup.service';
import { SettingItem, SettingTypes } from 'src/app/widget-settings/widget-settings.component';
import { Observer } from 'rxjs';

@Component({
  selector: 'app-widget-lbc-search',
  templateUrl: './widget-lbc-search.component.html',
  styleUrls: ['./widget-lbc-search.component.scss']
})
export class WidgetLbcSearchComponent extends WidgetCompBase implements OnInit {
  data: any = {};

  constructor(api: ApiService,
      eventService: EventService,
      timer: TimerService,
      popup: GlobalPopupService) {
    super(eventService, timer, popup, api);

    this.settings["numberOf"] = new SettingItem(SettingTypes.NUMBER, {
      displayName: "Number of items displayed"
    }, 5);
    this.settings["search"] = {
      term: new SettingItem(SettingTypes.STRING, {
        displayName: "Search terms"
      }, ""),
      category: new SettingItem(SettingTypes.AUTO, {
        displayName: "Category",
        optionsUrl: "/api/leboncoin/categories"
      }, "MULTIMEDIA"),
      region: new SettingItem(SettingTypes.AUTO, {
        displayName: "Region",
        optionsUrl: "/api/leboncoin/regions"
      }, "Languedoc-Roussillon")
    }
    this.settings["search"]["region"].notSerializable = {
      data: this._api.settingsGetOptions(this.settings["search"]["region"].options.optionsUrl)
    }
    this.settings["search"]["category"].notSerializable = {
      data: this._api.settingsGetOptions(this.settings["search"]["category"].options.optionsUrl)
    }
  }

  openUrl(url: string) {
    var win = window.open(url, '_blank');
    win.focus();
  }

  ngOnInit() {
    this.listen((observer: Observer<any>) => {
      this._api.leboncoin.search(this.settings.search.term, this.settings.search.category, this.settings.search.region, this.settings.numberOf).subscribe((data: any) => {
        observer.next(data.data);
      });
    }, (result: any) => {
      this.loaded = true;
      this.data = result;
    });
  }

}
