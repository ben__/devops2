import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetLbcSearchComponent } from './widget-lbc-search.component';

describe('WidgetLbcSearchComponent', () => {
  let component: WidgetLbcSearchComponent;
  let fixture: ComponentFixture<WidgetLbcSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetLbcSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetLbcSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
