import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetUserEpitechComponent } from './widget-user-epitech.component';

describe('WidgetUserEpitechComponent', () => {
  let component: WidgetUserEpitechComponent;
  let fixture: ComponentFixture<WidgetUserEpitechComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetUserEpitechComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetUserEpitechComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
