import { Subscription, Observer } from "rxjs";
import { OnDestroy, OnInit } from '@angular/core';
import { TimerService } from "src/app/Services/timer.service";
import * as _ from "lodash";
import { GlobalPopupService } from '../../global-popup/global-popup.service';
import { WidgetSettingsComponent, SettingItem, SettingTypes } from "src/app/widget-settings/widget-settings.component";
import { PopupResponse, PopupResponseType } from '../../global-popup/global-popup.component';
import { EventService } from '../../Services/event.service';
import { ApiService } from "src/app/Services/api.service";

export abstract class WidgetCompBase {
  protected _subscriptions = new Array<Subscription>();
  protected OverloadedOnInit?: () => void;
  protected OverloadedOnDestroy?: () => void;

  constructor(
    protected _eventService: EventService,
    protected _timer: TimerService,
    private   _popup: GlobalPopupService,
    protected _api: ApiService,
    public    loaded = false) {}

  listen<T>(request: (observer: Observer<any>) => void, handler: (data: T) => void) {
    let obs = this._timer.listen(this, request).obs;
    this._subscriptions.push(obs.subscribe(handler));
    return (obs);
  }

  destroy() {
    _.each(this._subscriptions, (sub) => {
      sub.unsubscribe();
    })
    if (typeof this.OverloadedOnDestroy !== "undefined") {
      this.OverloadedOnDestroy();
    }
  }

  showSettings() {
    let instance = this._popup.fire(WidgetSettingsComponent, _.cloneDeep(this.settings));
  
    instance.registerOnClose((res: PopupResponse<any>) => {
      if (res.type === PopupResponseType.ACCEPT) {
        this.settings = res.data;
        this._eventService.get("onSettingsChange").emit();
        this._timer.forceRefresh(this);
      }
    })
    return instance;
  }

  loadSettings(data, settings = this.settings) {
    _.each(data, (value, key) => {
      if (value._type !== undefined) {
        settings[key] = new SettingItem(value._type, value.options, value._data)
        if (value._type == SettingTypes.AUTO) {
          settings[key].notSerializable = {
            data: this._api.settingsGetOptions(value.options.optionsUrl)
          }
        }
      } else {
        settings[key] = this.loadSettings(value, {});
      }
    });
    return settings;
  }
  
  public settings = {
    intervalMs: new SettingItem(SettingTypes.NUMBER, {
      displayName: "Refresh interval (ms)"
    }, 10000),
  } as any;
};