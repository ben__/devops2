import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { GlobalPopupComponent } from './global-popup/global-popup.component';
import { DashboardMainComponent } from './dashboard-main/dashboard-main.component';
import { LoginViewComponent } from './login-view/login-view.component';
import { GlobalPopupBlurDirective } from './global-popup/global-popup.service';
import { WidgetSelectorComponent } from './widget-selector/widget-selector.component';
import { WidgetSelectorDirective } from './dashboard-main/widget-selector.directive';
import { WidgetSettingsComponent } from './widget-settings/widget-settings.component';


import { WidgetWeatherComponent } from './dashboard-main/Widgets/widget-weather/widget-weather.component';
import { WidgetEpitechModuleComponent } from './dashboard-main/Widgets/widget-epitech-module/widget-epitech-module.component';
import { WidgetLbcSearchComponent } from './dashboard-main/Widgets/widget-lbc-search/widget-lbc-search.component';
import { WidgetUserEpitechComponent } from './dashboard-main/Widgets/widget-user-epitech/widget-user-epitech.component';
import { WidgetRssComponent } from './dashboard-main/Widgets/widget-rss/widget-rss.component';
import { WidgetAtomComponent } from './dashboard-main/Widgets/widget-atom/widget-atom.component';
import { WidgetYoutubeChannelComponent } from './dashboard-main/Widgets/widget-youtube-channel/widget-youtube-channel.component';

import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DragAndDropModule } from 'angular-draggable-droppable';
import {
  MatCardModule,
  MatInputModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatTabsModule,
  MatButtonModule,
  MatToolbarModule,
  MatProgressBarModule,
  MatIconModule,
  MatSidenavModule,
  MatTooltipModule,
  MatExpansionModule,
  MatGridListModule,
  MatProgressSpinnerModule,
  MatTreeModule,
  MatOptionModule,
  MatSelectModule
} from '@angular/material';


@NgModule({
  declarations: [
    AppComponent,
    GlobalPopupComponent,
    DashboardMainComponent,
    LoginViewComponent,
    WidgetSelectorComponent,
    WidgetSettingsComponent,

    GlobalPopupBlurDirective,
    WidgetSelectorDirective,

    WidgetWeatherComponent,
    WidgetEpitechModuleComponent,
    WidgetLbcSearchComponent,
    WidgetUserEpitechComponent,
    WidgetRssComponent,
    WidgetAtomComponent,
    WidgetYoutubeChannelComponent
  ],
  entryComponents: [
    LoginViewComponent,
    WidgetSelectorComponent,
    WidgetSettingsComponent,

    WidgetWeatherComponent,
    WidgetLbcSearchComponent,
    WidgetEpitechModuleComponent,
    WidgetUserEpitechComponent,
    WidgetRssComponent,
    WidgetAtomComponent,
    WidgetYoutubeChannelComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    DragAndDropModule,

    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatTabsModule,
    MatButtonModule,
    MatToolbarModule,
    MatProgressBarModule,
    MatIconModule,
    MatSidenavModule,
    MatCardModule,
    MatTooltipModule,
    MatExpansionModule,
    MatGridListModule,
    MatProgressSpinnerModule,
    MatTreeModule,
    MatOptionModule,
    MatSelectModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
