import { GlobalPopupComponent, PopupProperty } from './global-popup.component';
import { Injectable, Directive, ElementRef } from '@angular/core';

import * as _ from "lodash";

@Injectable({
  providedIn: 'root'
})
export class GlobalPopupService {
  private _isReady: boolean = false;
  private _ready: Function;
  private _popupRoot: GlobalPopupComponent;
  private _blurBack: GlobalPopupBlurDirective;
  private _counter: number = 0;

  constructor() {}

  private _counterIncr() {
    if (this._counter <= 0) {
      this._popupRoot.show();
      this._blurBack.blur(true);
    }
    this._counter++;
  }

  private _counterDecr() {
    this._counter = (this._counter > 0
                        ? this._counter - 1
                        : 0);
    if (this._counter <= 0) {
      this._popupRoot.hide();
      this._blurBack.blur(false);
    }
  }

  register(root: GlobalPopupComponent) {
    this._popupRoot = root;
    if (this._isReady == false) {
      this._isReady = true;
      if (typeof this._ready !== "undefined")
        _.defer(this._ready.bind(this));
    }
  }

  registerBack(blurBack: GlobalPopupBlurDirective) {
    this._blurBack = blurBack;
  }

  fire(component: any, ...args: any[]): PopupProperty {
    if (this._isReady == false)
      return;
    this._counterIncr();
    let comp = this._popupRoot.onAddPopup(component, ...args);
    comp.registerOnClose(() => { this._counterDecr(); });
    return comp;
  }

  ready(hdl: Function) {
    if (this._isReady)
      hdl();
    else
      this._ready = hdl;
  } 
}

@Directive({
  selector: "[app-global-popup-blur]",
})
export class GlobalPopupBlurDirective {
  constructor(private _me: ElementRef,
              _service: GlobalPopupService) {
    _service.registerBack(this);
    _me.nativeElement.style.display = "block";
    _me.nativeElement.style.transition = "filter 500ms";
  }

  blur(enabled: boolean) {
    let me: HTMLElement = this._me.nativeElement;

    if (enabled)
      me.style.filter = "blur(3rem)";
    else
      me.style.filter = "";
  }
}