import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { PopupProperty } from './../global-popup/global-popup.component';
import { ApiService } from '../Services/api.service';
import * as Protocol from '../../Protocol/Authentification';
import * as _ from 'lodash'
import { ResponseType } from '../../Protocol/Protocol';

@Component({
  selector: 'app-login-view',
  templateUrl: './login-view.component.html',
  styleUrls: ['./login-view.component.scss']
})
export class LoginViewComponent extends PopupProperty implements OnInit, AfterViewInit {
  public displayProgressBar = false;
  public register = {
    username: "",
    password: "",
    confirmPassword: "",
    statusText : ""
  };
  public login = {
    username: "",
    password: "",
    statusText: ""
  };
  public statusText = "";

  @ViewChild("UsernameInput") usernameInput: ElementRef
  @ViewChild("PasswordInput") passwordInput: ElementRef

  constructor(private _api: ApiService) {
    super();
  }

  ngAfterViewInit() {
    _.defer(() => {
      let l = localStorage.getItem("lastUserName");
      if (typeof l === "string") {
        this.login.username = l;
        this.passwordInput.nativeElement.focus();
      } else {
        this.usernameInput.nativeElement.focus();
      }
    });
  }

  onLoginClicked() {
    this.login.statusText = "";
    this.displayProgressBar = true;
    this._api.login(this.login.username, this.login.password).subscribe((resp) => {
      if (resp.type == ResponseType.ACCEPT) {
        localStorage.setItem("lastUserName", this.login.username);
        this.accept();
      } else {
        this.login.statusText = resp.message;
      }
      this.displayProgressBar = false;
    }, (error) => {
      this.login.statusText = "Server Error: " + error.message;
      this.displayProgressBar = false;
    });
  }

  onRegisterClick() {
    this.register.statusText = "";
    if (this.register.password !== this.register.confirmPassword) {
      this.register.statusText = "passwords not matching";
      return;
    }

    this.displayProgressBar = true;
    this._api.register(this.register.username, this.register.password).subscribe((resp) => {
      if (resp.type == ResponseType.ACCEPT) {
        this.login.username = this.register.username;
        this.login.password = this.register.password;
        this.onLoginClicked();
      } else {
        this.register.statusText = resp.message;
      }
      this.displayProgressBar = false;
    }, (error) => {
      this.register.statusText = "Server Error: " + error.message;
      this.displayProgressBar = false;
    });
  }


  ngOnInit() {}

}

export let LoginViewGetter = () => {
  return LoginViewComponent;
}
