Twitter
	dernier tweet d’une personne P suivie
	dernier d’un # X
intra
	prochaines activités d’un module M

yammer
	dernier post dans un groupe G
	dernier post d’une personne P

Youtube
	dernière vidéo d’une personne P suivie
	nombre d’abonnés d’une personne P suivie
	nombre de vue sur votre vidéo V

Mail
	gestionnaire de mail

RSS
	Affichage de la liste des N derniers articles pour le flux F

Météo
	Affichage de la température pour une ville V

Horloge
	heure d’un pays P

RandomWidget
	affichage d’un mot M random de N charactères
	affichage d’une page random de Wikipedia en partant de la page P

responsive mobile
AWS