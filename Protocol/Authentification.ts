import { ResponseType } from './Protocol';

export namespace Login {
  export interface Request {
    name: string,
    pass: string //Hash
  }
  
  export interface Response {
    type: ResponseType,
    message: string
  }

  export interface CheckResponse {
    type: ResponseType
    username?: string,
    message?: string
  }
}

export namespace Register {
  export interface Request {
    name: string,
    pass: string
  }

  export interface Response {
    type: ResponseType;
    message: string;
  }
}