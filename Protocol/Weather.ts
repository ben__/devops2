import * as ProtocolBase from './Protocol';

export class WeatherCityResponse extends ProtocolBase.BasicResponse {
  data: string;
}