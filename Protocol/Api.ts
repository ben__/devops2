import * as ProtocolBase from './Protocol';

export class WidgetSheetResponse extends ProtocolBase.BasicResponse {
  sheet?: Array<{}>
}